import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    cart: []
  },
  mutations: {
    addToCart (state, data) {
      console.log(data)
      let selectedItem = data.selecteditem

      if (selectedItem.quantity === 0) {
        return false
      }

      let a = state.cart.findIndex(x => x.name === selectedItem.name)

      if (a === -1) {
        selectedItem.quantity = 1
        state.cart.push(selectedItem)
      } else {
        if (selectedItem.quantity === state.cart[a].quantity) {
          return false
        }

        state.cart[a].quantity = parseInt(state.cart[a].quantity) + 1
      }
    },
    deleteItems (state, items) {
      state.cart.splice(items, 1)
    }
  },
  getters: {
    cartCount: state => state.cart.length
  },
  actions: {
  },
  modules: {
  }
})
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
